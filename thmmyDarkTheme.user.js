// ==UserScript==
// @name        thmmyDarkTheme
// @namespace   thmmyDarkTheme
// @include     www.thmmy.gr/*
// @include     https://www.thmmy.gr/*
// @resource    customCss thmmyDarkTheme.css
// @version     1
// @grant       GM_addStyle
// @grant       GM_getResourceText
// @run-at document-start
// ==/UserScript==

//Hide all
var newstyle = document.createElement("style");
newstyle.setAttribute("type", "text/css");
newstyle.id = 'hideAll';
newstyle.innerHTML ="body{background-color: #3c3f41;} body *{display:none;}";
document.documentElement.appendChild(newstyle);


document.addEventListener('DOMContentLoaded', function (){
//Add own stylesheet
var cssTxt  = GM_getResourceText ("customCss");
GM_addStyle (cssTxt);

//Add link to FontAwesome
var link = document.createElement('link');
link.id = 'id2';
link.rel = 'stylesheet';
link.href = 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css';
document.head.appendChild(link);

//Remove 100% width to avoid overflow
document.getElementById('bodyarea').width = "";

//Hide those two buttons
document.getElementById('main').children[1].style.display = 'none';
document.getElementById('main').children[2].style.display = 'none';

var leftbarHeader = document.getElementById('leftbarHeader');
if (leftbarHeader !== null) {
  //Hide the "Style" element from the sidebar
  leftbarHeader.children[7].style.display = 'none';
  //Hide "Tools" element from the sidebar
  leftbarHeader.children[3].style.display = 'none';
  //Hide "Search" element from the sidebar
  leftbarHeader.children[4].style.display = 'none';
}

//Outer sidebar styling
buildTopBar();

//ShoutBox styling
var shoutBox = document.getElementById('leftbarHeader').children[1];
shoutBox.style.width = '16%';
shoutBox.style.position = 'fixed';
shoutBox.style.bottom = '0';
shoutBox.style.right = '1%';
document.getElementById('block6').getElementsByTagName('table') [0].getElementsByTagName('div') [0].style.height = '320px';
document.getElementById('block6').children[2].getElementsByTagName('textarea') [0].style.width = '95%';
document.getElementById('block6').children[2].getElementsByTagName('textarea') [0].style.resize = 'none';
document.getElementsByTagName('body') [0].appendChild(shoutBox);

//Add a div to each post for the card style shadow
if (window.location.href.indexOf("topic=") > -1) {
  if (window.location.href.indexOf("action=post") <= -1) {
    var posts = document.querySelectorAll('table.bordercolor>tbody>tr>td');
    for (i = 0; i < posts.length-1; i++) {
      var postInnerTable = posts[i].getElementsByTagName('table')[0];
      var postOuterDiv = document.createElement('div');
      postOuterDiv.appendChild(postInnerTable);
      postOuterDiv.className = "custom_post_div";
      posts[i].appendChild(postOuterDiv);
    }
  }
}

document.getElementById('hideAll').innerHTML ="";;

function buildTopBar() {
  //Build div
  var topMenu = document.createElement('div');
  topMenu.appendChild(document.createTextNode(''));
  topMenu.className = 'top_menu';
  document.getElementsByTagName('body') [0].appendChild(topMenu);

  //Insert ul
  var menuItemList = document.createElement('ul');
  menuItemList.appendChild(document.createTextNode(''));
  topMenu.appendChild(menuItemList);

  var i,
  topMenuItemAnchorIcon = [
  'fa fa-newspaper-o fa-2x',
  'fa fa-home fa-2x',
  'fa fa-question-circle fa-2x',
  'fa fa-search fa-2x',
  'fa fa-calendar fa-2x',
  'fa fa-picture-o fa-2x',
  'fa fa-sign-out fa-2x',
  document.getElementById('ava').children[0].getAttribute('src')
  ],
  topMenuItemAnchorHRef = [
  'https://www.thmmy.gr/smf/index.php',
  'https://www.thmmy.gr/smf/index.php?action=forum',
  'https://www.thmmy.gr/smf/index.php?action=help',
  'https://www.google.gr/search?q=&sitesearch=www.thmmy.gr&gws_rd=cr&ei=h9RsWfTcFcqk6ASB6JvgAg',
  'https://www.thmmy.gr/smf/index.php?action=calendar',
  'https://www.thmmy.gr/smf/index.php?action=gallery',
  document.getElementById('logoutbtn').getAttribute('href'),
  'https://www.thmmy.gr/smf/index.php?action=profile'
  ],topMenuItemAnchorTitles = [
  'Home',
  'Forum',
  'Help',
  'Search',
  'Calendar',
  'Gallery',
  'Logout',
  'Profile'
  ];

  for (i = 0; i < 8; i++) {
    var menuItem = document.createElement('li');
    menuItem.appendChild(document.createTextNode(''));
    if (i<6){
      menuItem.style.float = 'left';
    } else{
      menuItem.style.float = 'right';
    }
    menuItemList.appendChild(menuItem);
    
    var menuItemAnchor = document.createElement('a');
    menuItemAnchor.appendChild(document.createTextNode(''));
    menuItemAnchor.className = 'top_menu_item_anchor';
    menuItemAnchor.href = topMenuItemAnchorHRef[i];
    menuItem.appendChild(menuItemAnchor);
    if (i == 3){
      menuItemAnchor.target = "_blank";
    }
    
    if(i<7){
      var menuItemAnchorIcon = document.createElement('i');
      menuItemAnchorIcon.appendChild(document.createTextNode(''));
      menuItemAnchorIcon.className = topMenuItemAnchorIcon[i];
      menuItemAnchorIcon.title = topMenuItemAnchorTitles[i];
      menuItemAnchor.appendChild(menuItemAnchorIcon);
    }else{

      var menuItemAnchorIcon = document.createElement('img');
      menuItemAnchorIcon.appendChild(document.createTextNode(''));
      menuItemAnchorIcon.src = topMenuItemAnchorIcon[i];
      menuItemAnchorIcon.alt = topMenuItemAnchorTitles[i];
      menuItemAnchorIcon.style.maxHeight = "26px";
      menuItemAnchor.appendChild(menuItemAnchorIcon);
    }
  }
}
}, false);