This is a simple script that changes the layout and colors of thmmy.gr forum.  
It is build to work with Greasemonkey add-on in Firefox but it could work with  
other browsers too after some easy changes in the code.

## Requirements

The selected theme has to be Scribbles2.

---

**The site without the script:**

<img src="img/before.png" alt="Before" style="width: 200px;"/>  

**And with it:**

<img src="img/after.png" alt="After" style="width: 200px;"/>

## Installation

In Firefox download and install Greasemonkey add-on. Then drag
"thmmyDarkTheme.user.js" file and drop it in Firefox. On the dialog click Install.